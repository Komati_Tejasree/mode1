package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/***
 * @author tejasree and Vineela
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.dao.PetDao;
import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;
import com.casestudy.validator.LoginValidator;
import com.casestudy.validator.PetValidator;
import com.casestudy.validator.UserValidator;

@Controller
public class MainController {

	@Autowired
	public UserService userService;

	@Autowired
	private UserValidator userValidator;

	@Autowired
	private PetService petService;

	@Autowired
	private PetDao petDao;

	@Autowired
	private PetValidator petValidator;

	@Autowired
	private Pet pet;

	@Autowired
	private User user;

	@Autowired
	private LoginValidator loginValidator;

	// Index Page
	@RequestMapping(value = "/")
	public ModelAndView index() {

		ModelAndView modelAndView = new ModelAndView("registrationPage");

		modelAndView.addObject("user", user);

		return modelAndView;

	}

	// RegistrationPage
	@RequestMapping(value = "/register")
	public ModelAndView register() {

		ModelAndView modelAndView = new ModelAndView("registrationPage");
		return modelAndView;
	}

	// LoginPage
	@RequestMapping("/login")
	public ModelAndView login() {

		User user = new User();
		ModelAndView modelAndView = new ModelAndView("loginPage");
		modelAndView.addObject("userModel", user);

		return modelAndView;

	}

	// Home Page
	@RequestMapping(value = "/home")
	public ModelAndView home(@ModelAttribute("userModel") User user) {

		List<Pet> list = petService.getAllPets();
		ModelAndView modelAndView = new ModelAndView("homePage");
		modelAndView.addObject("petList", list);

		return modelAndView;

	}

	// save the User details
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") User user, BindingResult result, Model map) {

		userValidator.validate(user, result);
		userValidator.validatePassword(user, result);
		// userValidator.validateUser(user, result);

		ModelAndView modelAndView = null;
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("registrationPage");

		} else {
			userService.saveUser(user);
			User user1 = new User();
			map.addAttribute("loginModel", user1);
			modelAndView = new ModelAndView("redirect:/login");
		}
		return modelAndView;
	}

	//AuthenticateUser
	@RequestMapping(value = "/authenticateUser")
	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("loginModel") User user, BindingResult result) {

		ModelAndView modelAndView = null;
		loginValidator.validate(user, result);
		loginValidator.authenticate(user, result);
		if (result.hasErrors()) {
			modelAndView = new ModelAndView("loginPage");
		} else {
			modelAndView = new ModelAndView("redirect:/home");
		}

		return modelAndView;

	}

	//AddPet 
	@RequestMapping(value = "/addPetPage")
	public ModelAndView addPet() {

		ModelAndView modelAndView = new ModelAndView("addPetPage");

		modelAndView.addObject("pet", pet);

		return modelAndView;

	}

	//save the pet details 
	@RequestMapping(value = "/savePet")
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet, BindingResult result) {
		petValidator.validate(pet, result);
		petValidator.validateAge(pet, result);
		ModelAndView modelAndView = null;

		if (result.hasErrors()) {
			modelAndView = new ModelAndView("addPetPage");
		}

		else {
			petService.savePet(pet);
			modelAndView = new ModelAndView("homePage");
		}

		return modelAndView;

	}

	//MyPet page
	@GetMapping(value = "/myPet")
	public ModelAndView myPet(HttpServletRequest request) {

		petService.getMyPets(2);
		ModelAndView modelAndView = new ModelAndView("myPetPage");

		return modelAndView;

	}

	//BuyPet 
	@GetMapping(value = "/buyPet")
	public ModelAndView buyPet(HttpServletRequest request) {

		petService.buyPet(2, 1);
		ModelAndView modelAndView = new ModelAndView("redirect:/myPet");
		return modelAndView;
	}

	//Logout
	@RequestMapping(value = "/logout")
	public ModelAndView logout(HttpServletRequest request) {
		request.getSession().invalidate();
		ModelAndView modelAndView = new ModelAndView("redirect:/login");// session out and redirected to login page.

		return modelAndView;
	}

}
