package com.casestudy.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;

@Component
public class PetValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return Pet.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petName", "message");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petAge", "message");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "petPlace", "message");
       
	}

	public boolean validateAge(Pet pet, Errors errors) {
		if(pet.getPetAge()<0 || pet.getPetAge()>99) { 

			errors.reject("petAge","Age"); 
            return true;
			}else {
		return false; 
	}
}
}
	
