package com.casestudy.validator;

import org.springframework.stereotype.Component;

/***
 * @author tejasree
 */

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;

 
@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> arg0) {
        return User.class.equals(arg0);
    }


     // private String studentName; @ManyToOne @JoinColumn(name = "college_id_fk") private College college;

    
    @Override
    public void validate(Object arg0, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "msg");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "msg");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "msg");
    }

 

   public boolean validatePassword(User user, Errors errors) {
        if (!user.getUserPassword().equals(user.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "valid.passwordConfirm");
            return true;
        }else {
        return false;
    }

       /* @Transactional
        public boolean validateUser(User user, Errors errors) {
            Session session = this.sessionFactory.getCurrentSession();
            Query query = session.createQuery("select userName from User ");
            List<User> userList = query.list();
            if (userList.contains(user.getUserName())) {
                errors.rejectValue("userName", "valid.existing.user");
                return true;
            }

     

     

     

            return false;
        }
*/
     

     

     

    }






   }
