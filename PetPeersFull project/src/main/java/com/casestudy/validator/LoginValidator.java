package com.casestudy.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.UserService;
@Component
public class LoginValidator implements Validator {

	@Autowired
	private UserService userService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "message2");
	     ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPassword", "message2");
	     ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "message2");
		
	}

	public void authenticate(User user,Errors errors)
	{
		User user1 = userService.authenticateUser(user.getUserName(),user.getUserPassword());
		if(user1 == null)
		{
			 errors.rejectValue("userName","loginvalid");
			 errors.rejectValue("userPassword","loginvalid");
			
		}
	}
	
}
