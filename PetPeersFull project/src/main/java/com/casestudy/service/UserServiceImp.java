package com.casestudy.service;

/***
 * @author tejasree
 */
import java.util.List;

/***
 * @author tejasree
 */
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDao;
import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Service
@Transactional
public class UserServiceImp implements UserService {

	@Autowired
	UserDao userDao;

	@Override

	public User saveUser(User user) {
		userDao.saveUser(user);
		return user;
	}

	@Override
	public User authenticateUser(String userName, String userPassword) {
		return userDao.authenticateUser(userName, userPassword);

	}

}
