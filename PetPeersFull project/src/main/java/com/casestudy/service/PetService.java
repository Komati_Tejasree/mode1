package com.casestudy.service;
/***
 * @author Vineela
 */
import java.util.List;

import com.casestudy.model.Pet;

public interface PetService {
	public abstract Pet savePet(Pet pet);
	public abstract List<Pet> getAllPets();
	public List<Pet> getMyPets(int petId);
	public Pet buyPet(int petId, int userId);
}
