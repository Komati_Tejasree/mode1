package com.casestudy.dao;
/***
 * author Tejasree
 */
import java.util.List;

import com.casestudy.model.Pet;
/***
 * @author tejasree
 */
import com.casestudy.model.User;

public interface UserDao {
	public abstract User saveUser(User user);
   public abstract User authenticateUser(String userName,String userPassword);
	
}
