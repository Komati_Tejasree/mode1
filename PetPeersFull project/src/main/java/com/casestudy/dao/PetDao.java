package com.casestudy.dao;

/***
 * Vineela
 */
import java.util.List;

import com.casestudy.model.Pet;

public interface PetDao {
	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets();

	public List<Pet> getMyPets(int petId);

	public Pet buyPet(int petId, int userId);
}
