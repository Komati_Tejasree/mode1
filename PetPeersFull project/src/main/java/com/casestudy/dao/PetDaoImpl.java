package com.casestudy.dao;

/***
 * author Vineela
 */
import java.util.List;
import org.hibernate.Session;

import org.hibernate.SessionFactory;

import org.hibernate.query.Query;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;

@Repository

public class PetDaoImpl implements PetDao {

	@Autowired

	private SessionFactory sessionFactory;

	@Override

	public List<Pet> getAllPets() {

		Session session = sessionFactory.getCurrentSession();

		Query<Pet> query = session.createQuery("from pet", Pet.class);

		List<Pet> listOfPets = (query).getResultList();

		return listOfPets;

	}

	@Override

	public List<Pet> getMyPets(int petId) {

		Session session = sessionFactory.getCurrentSession();
		Query<Pet> query = session.createQuery("from Pet p where p.petId=?", Pet.class);
		query.setParameter(petId, query);
		List<Pet> PetIds = (query).getResultList();

		return PetIds;

	}

	@Override

	public Pet savePet(Pet pet) {

		Session session = sessionFactory.getCurrentSession();

		session.saveOrUpdate(pet);

		return pet;

	}

	@Override

	public Pet buyPet(int petId, int userId) {

// TODO Auto-generated method stub 

		return null;

	}

}
