package com.casestudy.dao;
/***
 * @Author Tejasree
 */
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

/***
 * @author tejasree
 */


import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
	
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public User saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return user;
		
	}
	@Override
	public User authenticateUser(String userName, String userPassword) {
		return null;
	
	}
	
}
