<!-- Author Vineela -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title> My Pets </title>
</head>
<body>
 
<%--    <jsp:include page="addpet.jsp" /> --%>
<%--    <jsp:include page="_menu.jsp" /> --%>
    
   
 
   <div >Pet Info</div>
 
   <div >
       <h3> Pet Information:</h3>
       <ul>
           <li>PetId: $pet.petId}</li>
           <li>PetName: ${pet.petName}</li>
           <li>PetAge: ${pet.petAge}</li>
           <li>PetPlace: ${pet.petPlace}</li>
       </ul>
       <h3> My Pets Summary:</h3>
       <ul>
           <li>Total:
           <span >
           <fmt:formatNumber value="${myPet.amount}" type="currency"/>
           </span></li>
       </ul>
   </div>
    
   <br/>
    
   <table border="1" style="width:100%">
       <tr>
           <th> Pet Id</th>
           <th> Pet Name</th>
           <th> Amount</th>
       </tr>
       <c:forEach items="${pet.details}" var="petDetailInfo">
           <tr>
               <td>${pet.petId}</td>
               <td>${pet.petName}</td>
               <td>${pet.quanity}</td>
               
               <td>
                <fmt:formatNumber value="${pet.amount}" type="currency"/>
               </td>  
           </tr>
       </c:forEach>
   </table>
   <c:if test="${pet.totalPages > 1}">
       <div>
          <c:forEach items="${paginationResult.navigationPages}" var = "page">
              <c:if test="${page != -1 }">
                <a href="petsList?page=${page}" class="nav-item">${page}</a>
              </c:if>
              <c:if test="${page == -1 }">
                <span class="nav-item"> ... </span>
              </c:if>
          </c:forEach>
            
       </div>
   </c:if>