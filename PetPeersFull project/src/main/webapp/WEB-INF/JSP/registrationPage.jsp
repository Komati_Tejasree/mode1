<!-- @Author Tejasree -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
<style type="text/css">

/*  td { white-space:pre } */
.error {
	color: red;
	font-size: medium;
}
</style>

</head>


<body>
	<div style="background-color: black; color: white; padding: 2px;">
		<h3>Pet Shop</h3>
	</div>

	<form:form action="saveUser" modelAttribute="user" method="post">
		<table align="center">



			<tr>
				<th>Register</th>
			</tr>

			<tr>
				<td>User Name : </td>
				</tr>
				<tr>
				<td><form:input path="userName" type ="text" size="100%"/> <br></td>
				</tr>
				<tr>
				<td><form:errors path="userName" cssClass="error"></form:errors></td>
			</tr>
			<tr>
				<td>User Password :</td>
				</tr>
				<tr>
				<td><form:input path="userPassword" type="password" size="100%" /></td>
				</tr>
				<tr>
				<td><form:errors path="userPassword" cssClass="error"></form:errors></td>
			</tr>
			
			<tr>
				<td>confirm Password :</td>
				</tr>
				<tr>
				<td><form:input path="confirmPassword" type="password" size="100%"/></td>
				</tr>
				<tr>
				<td><form:errors path="confirmPassword" cssClass="error"></form:errors></td>
			</tr>
			
			<tr>
				<td><input type="submit" value="Register" />
				<td></td>
			</tr>
			
				
		</table>
	</form:form>

</body>
</html>