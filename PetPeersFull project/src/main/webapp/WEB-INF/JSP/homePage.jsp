<!-- Author Tejasree and Vineela -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 

pageEncoding="ISO-8859-1"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<%@page import="java.sql.DriverManager"%> 

<%@page import="java.sql.ResultSet"%> 

<%@page import="java.sql.Statement"%> 

<%@page import="java.sql.Connection"%> 

<% 

String id = request.getParameter("userid"); 

String driver = "com.mysql.jdbc.Driver"; 

String connectionUrl = "jdbc:mysql://localhost:3306/"; 

String database = "petpeers"; 

String userid = "root"; 

String password = "root"; 

try { 

Class.forName(driver); 

} catch (ClassNotFoundException e) { 

e.printStackTrace(); 

} 

Connection connection = null; 

Statement statement = null; 

ResultSet resultSet = null; 

%> 

<!DOCTYPE html> 

<html> 

<head> 

<meta charset="ISO-8859-1"> 

<title>HomePage</title> 

<style> 

.center { 

margin: 50px 300px; 

} 

 

.right { 

position: absolute; 

right: 325px; 

color: white; 

} 

 

table { 

border-spacing: 0 10px; 

} 

 

a { 

color: white; 

text-decoration: none; 

} 

 

ul { 

list-style-type: none; 

margin: 0; 

padding: 0; 

overflow: hidden; 

background-color: #333; 

} 

 

li { 

float: left; 

} 

 

li { 

display: block; 

color: white; 

text-align: center; 

padding: 14px 16px; 

text-decoration: none; 

} 

 

li a:hover { 

background-color: #111; 

} 

 

.pettable { 

border-collapse: collapse; 

border-spacing: 0; 

border-color: #ccc; 

} 

 

.pettable td { 

font-family: Arial, sans-serif; 

font-size: 16px; 

padding: 16px 8px; 

overflow: hidden; 

word-break: normal; 

border- color: #ccc; 

color: black; 

} 

 

.pettable td a { 

color: blue; 

} 

 

.pettable th { 

font-family: Arial, sans-serif; 

font-size: 16px; 

font-weight: normal; 

padding: 16px 8px; 

border-style: solid; 

border-width: 2px; 

overflow: hidden; 

word-break: normal; 

border- color: #ccc; 

color: #000000; 

} 

/* .pettable .pettable-4eph{background-color:#C0C0C0} */ 

</style> 

</head> 

<body> 

<div class="center"> 

<ul> 

<li>Pet Shop</li> 

<li><a href="home">Home</a></li> 

<li><a href="addPetPage">Add Pet</a></li> 

<li><a href="myPetsPage">My Pet</a></li> 

<li><a href="login" class="right">Logout</a></li> 

</ul> 

</div> 

<table class="pettable" border="1" align="center"> 

<tr> 

<th width="100"><b>PET Id</b></th> 

<th width="100"><b>Name</b></th> 

<th width="100"><b>Age</b></th> 

<th width="100"><b>City</b></th> 

<th width="100"><b>Buy</b></th> 

</tr> 

<% 

            try{ 

            connection = DriverManager.getConnection(connectionUrl+database, userid, password); 

            statement=connection.createStatement(); 

            String sql ="select * from pettable"; 

            resultSet = statement.executeQuery(sql); 

            while(resultSet.next()){ 

            %> 

<tr> 

<td><%=resultSet.getString("petId") %></td> 

<td><%=resultSet.getString("petName") %></td> 

<td><%=resultSet.getString("petAge") %></td> 

<td><%=resultSet.getString("petPlace") %></td> 

<td><a href="<c:url value='/fi/${p.petId}'/>" id="bs" 

onclick="buyPet">Buy</a></td> 

</tr> 

<% 

            } 

            connection.close(); 

            } catch (Exception e) { 

            e.printStackTrace(); 

            } 

            %> 

</table> 

<script type="text/javascript"> 

  

</script> 

</body> 

</html> 