<!-- @Author Tejasree -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LoginPage</title>
<style type="text/css">

.error {
	color: red;
	font-size: medium;
}

.container {
  padding: 16px;
  font-size:large;
}
</style>
<body>
	<div style="background-color: black; color: white; padding: 2px;">
		<h3>PET SHOP</h3>
		<a href="homePage.jsp">Home</a></td>
	</div>
	<form:form action="home"  modelAttribute="userModel" method="post">
	
		<table align="center" >
			<tr>
				<th><h4>LoginPage</h4></th>
			</tr>
            <div class=container>
			<tr>
                <td><form:label path="userName">UserName:</form:label></td> 
                </tr>
                <tr>
				<td><form:input path="userName" required="required" size="100%"/> <br></td>
				</tr>
				<tr>
				<td><form:errors path="userName" cssClass="error"></form:errors></td>
			</tr>

			<tr>
				<td><form:label path="userPassword">UserPassword :</form:label></td><br/>
				</tr>
				<tr>
				<td><form:input path="userPassword" type="password" required="required" size="100%" /></td>
				</tr>
				<tr>
				<td><form:errors path="userPassword" cssClass="error"></form:errors></td>
				
			</tr>
            

			<tr>
				<td><input type="submit" value="Login">
<!-- 				<td>Not a member yet ? -->
<!-- 					<a href="register" class="to_register">Join us</a></td> -->
				</tr>
				</div>



					
				
				
<!-- 				<td><a href="homePage.jsp">Home</a></td> -->
				 <%-- 			
				<td><form:reset value="Reset" />Reset</td> --%>
		</table>
	</form:form>
</body>
</html>